import {query} from './query/index';
import {GraphQLSchema} from "graphql";


export let schema = new GraphQLSchema({
    query
});