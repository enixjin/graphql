import {userQuery} from "./UserQuery";
import {GraphQLObjectType} from "graphql";

export let query = new GraphQLObjectType({
    name: "query",
    description: "root api",
    fields: () => {
        return Object.assign({}, userQuery());
    }
});
