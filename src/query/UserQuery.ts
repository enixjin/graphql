import {db} from './../database/mockDB';
import {UserType} from './../type/UserType';
import {GraphQLList, GraphQLInt, GraphQLNonNull} from "graphql";

export let userQuery = () => ({
    users: {
        type: new GraphQLList(UserType),
        resolve: () => {
            return db;
        }
    },
    user: {
        type: UserType,
        args: {
            "id": {
                type: new GraphQLNonNull(GraphQLInt)
            }
        },
        resolve: (_, {
            id
        }) => {
            return db.find(u => u.id === id);
        }
    }
});