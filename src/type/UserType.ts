import {
    GraphQLInt,
    GraphQLObjectType,
    GraphQLString,
    GraphQLNonNull,
    GraphQLBoolean
} from "graphql";

export const UserType = new GraphQLObjectType({
    name: "user",
    fields: {
        id: {
            type: new GraphQLNonNull(GraphQLInt),
            description: "id of user"
        },
        username: {
            type: new GraphQLNonNull(GraphQLString),
            description: "username of user"
        },
        password: {
            type: new GraphQLNonNull(GraphQLString),
            args: {
                "hide": {
                    type: GraphQLBoolean,
                    defaultValue: true
                }
            },
            description: "password of user, default is hide",
            resolve: (_, {hide}) => {
                if (hide) {
                    return "******";
                } else {
                    return _.password;
                }
            }
        }
    }
});