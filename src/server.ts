import {schema} from "./schema";
import * as express from "express";
import * as graphqlHTTP from "express-graphql";

let port = 3000;

const app = express();

// check auth here
let checkAuth = (req, res, next) => {
    // console.log(req.headers.jwt);
    next();
};

app.use('/graphql', checkAuth, graphqlHTTP({
    schema: schema,
    // rootValue: root,
    graphiql: true //Set to false if you don't want graphiql enabled
}));

app.listen(port);
console.log('GraphQL API server running at localhost:' + port);