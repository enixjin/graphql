const nodeExternals = require('webpack-node-externals');
const webpack = require('webpack');
const path = require("path");

module.exports = {
  output: {
    filename: '[name].min.js',
    path: path.join(__dirname, "dist"),
    libraryTarget: 'umd',
    umdNamedDefine: true,
    library: 'gql',
  },
  entry: {
    gql: "./src/server.ts"
  },
  resolve: {
    extensions: [".ts"]
  },
  target: 'node',
  module: {
    rules: [{
        test: /\.ts$/,
        loader: "ts-loader",
        exclude: /node_modules/
      },
      {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader',
        exclude: [
          /node_modules/
        ]
      }
    ]
  },
  externals: [nodeExternals()],
  devtool: "source-map",
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      mangle: false,
      compress: {
        // drop_console: true
      }
    })
  ]
};